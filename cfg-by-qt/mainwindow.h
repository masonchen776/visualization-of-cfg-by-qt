#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QDir>
#include <QScrollArea>
#include <QVBoxLayout>
#include <iostream>
#include <fstream>
#include <QInputDialog>

#include <QMessageBox>
#include <cstring>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <QDebug>
using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void DrawSvg();

protected:

void  paintEvent(QPaintEvent *);

private slots:
    void FuncButtonClicked();//动态创建QPushButton按钮信号槽函数

    void on_DtcBtn_clicked();

    void on_SlcBtn_clicked();

private:
    Ui::MainWindow *ui;
    char svgPath[1024] = {0};
    char FolderPath[1024] = {0};
    char SolName[1024] = {0};
    int window_width, window_height;

    char srcDir[1024] = {0};
    char pathDir[1024] = {0};
    char dot_routeDir[1024] = {0};
    char svg_routeDir[1024] = {0};
};

#endif // MAINWINDOW_H
