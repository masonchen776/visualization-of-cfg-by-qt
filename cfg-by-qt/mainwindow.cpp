#include "mainwindow.h"
#include "ui_mainwindow.h"

//构造函数
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    update();
}

//析构函数
MainWindow::~MainWindow()
{
    delete ui;
    char cmd[1024] = {0};
    sprintf(cmd, (QString("rm -rf ")+QString(this->dot_routeDir)).toLatin1().data());
    system(cmd);
    sprintf(cmd, (QString("rm -rf ")+QString(this->svg_routeDir)).toLatin1().data());
    system(cmd);
}

//重构的绘图事件
void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QPixmap pixmap = QPixmap(":image/background.png").scaled(this->size());
    painter.drawPixmap(this->rect(), pixmap);

    QGraphicsScene *scene=new QGraphicsScene(); //创建场景，载入图像
    scene->addPixmap(QPixmap(this->svgPath));
    ui->graphicsView->setScene(scene); //用控件显示这个场景

    this->window_width = this->frameGeometry().width(); //获取ui形成窗口宽度
    this->window_height = this->frameGeometry().height();//获取窗口高度

    //当窗口大小发生变化时，让控件的位置大小也发生变化
    ui->SlcBtn->setGeometry(this->window_width*0.065, this->window_height*0.21, this->window_width*0.11, this->window_height*0.07);
    ui->DtcBtn->setGeometry(this->window_width*0.065, this->window_height*0.29, this->window_width*0.11, this->window_height*0.07);
    ui->scrollArea->setGeometry(this->window_width*0.065, this->window_height*0.37, this->window_width*0.11, this->window_height*0.43);
    ui->graphicsView->setGeometry(this->window_width*0.27, this->window_height*0.22, this->window_width*0.66, this->window_height*0.57);
}

void MainWindow::FuncButtonClicked()
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    //获取按钮显示文本
    QString text = btn->text();
    text = this->svg_routeDir+ QString("/") + text + QString(".svg");
    sprintf(this->svgPath, text.toLatin1().data());
    update();
}


void MainWindow::on_DtcBtn_clicked()
{
    FILE * fp;
    char cmd[1024] = {0} ;
    if(fp = fopen(this->pathDir,"r")){
        fclose(fp);
    }
    else{
        cout << "path file in not here" << endl;
        return ;
    }
    if(fp = fopen((QString(this->dot_routeDir)+QString("/path.dot")).toLatin1().data(),"w+")){
        fclose(fp);
    }
    else{
        sprintf(cmd, (QString("touch ")+QString(this->dot_routeDir)+QString("/path.dot")).toLatin1().data());
        system(cmd);
    }
    ifstream in(this->pathDir);
    ofstream out((QString(this->dot_routeDir)+QString("/path.dot")).toLatin1().data());
    char cur_Func[100] = {0} ;
    char line[1024] = {0} ;
    out << "digraph G{\n  rankdir = LR ;\n  subgraph {\n" ;
    in.getline(line,1024);
    while(strlen(line)!=0){
        if(line[0]=='0'){
            //if is a node
            char node_Func[100] = {0};
            char node_Num[6] = {0};
            char node_Type[3] = {0};
            int i = 3 ;
            while(line[i]!='\"'){
                node_Func[i-3] = line[i];
                i ++ ;
            }
            //get node func
            if(strncmp(node_Func,cur_Func,100)!=0){
                out << "  }\n" ;
                strncpy(cur_Func,node_Func,100);
                out << "  subgraph " << cur_Func << " {\n" ;
            }
            i += 2;
            int flag = i ;
            while(line[i]!=' '){
                node_Num[i-flag] = line[i];
                i ++ ;
            }
            //get node num
            out << "  " << node_Num << " ";
            i ++ ;
            flag = i ;
            while(line[i]!='\0'){
                node_Type[i-flag] = line[i] ;
                i ++ ;
            }
            //get node type
            switch (atoi(node_Type)) {
            case 0:
                out << "[shape = box, label = \"";
                break;
            case 1:
                out << "[shape = Mrecord, label = \"" << cur_Func << "'s entry\\n";
                break;
            case 2:
                out << "[shape = diamond, label = \"";
                break;
            default:
                break;
            }

            //and then get code
            in.getline(line,1024);
            if(line[0]==' '){
                out << line << "\\n" ;
                in.getline(line,1024);
            }
            out << "\"];\n" ;
        }
        else{
            //if is a edge
            int i = 2 ;
            char from_Node_Num[6] = {0} ;
            char to_Node_Num[6] = {0} ;
            while(line[i]!=' '){
                from_Node_Num[i-2] = line[i] ;
                i ++ ;
            }
            i++;
            int flag = i ;
            while(line[i]!='\0'){
                to_Node_Num[i-flag] = line[i] ;
                i ++ ;
            }
            out << "  " << from_Node_Num << "->" << to_Node_Num << " [style = solid, label = \"" ;
            in.getline(line,1024);
            if(line[0]==' '){
                out << line << "\\n" ;
                in.getline(line,1024);
            }
            out << "\"];\n" ;
        }
    }

    out << "  }\n}" << endl ;
    in.close();
    out.close();
    sprintf(cmd,(QString("dot -Tsvg ")+QString(this->dot_routeDir)+QString("/path.dot -o ")+QString(this->svg_routeDir)+QString("/path.svg")).toLatin1().data());
    system(cmd);
    sprintf(this->svgPath, (this->svg_routeDir+ QString("/path.svg")).toLatin1().data());
    update();
}

void MainWindow::on_SlcBtn_clicked()
{
    bool isOk;
    QString text=QInputDialog::getText(this,QString("Get Sol File"),
                                       QString("Input Sol Path"), QLineEdit::Normal, QDir::home().dirName(), &isOk);

    if(isOk&&!text.isEmpty())
    {
        char t[1024] = {0};
        sprintf(t, text.toLatin1().data());
        for(int i = text.length(); i >= 0; i --){
            if(text[i]=='/'){
                strncpy(this->FolderPath, t, i+1);
                strncpy(this->SolName, t+i+1, text.length()-i);
                break;
            }
        }
    }

    QString folder = QString(this->FolderPath);
    sprintf(this->srcDir, (folder + QString("all.txt")).toLatin1().data());
    sprintf(this->pathDir, (folder + QString("path.txt")).toLatin1().data());
    sprintf(this->dot_routeDir, (folder + QString("dot")).toLatin1().data());
    sprintf(this->svg_routeDir, (folder + QString("svg")).toLatin1().data());
    char cmd[1024] = {0};
    sprintf(cmd, (QString("mkdir ")+QString(this->dot_routeDir)).toLatin1().data());
    system(cmd);
    sprintf(cmd, (QString("mkdir ")+QString(this->svg_routeDir)).toLatin1().data());
    system(cmd);

    this->DrawSvg();
}

void MainWindow::DrawSvg() {
    QScrollArea* m_pChatListScrollArea = ui->scrollArea;
    QVBoxLayout* m_pSCVLayout;

    m_pChatListScrollArea->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
    m_pChatListScrollArea->setWidgetResizable(true);

    m_pSCVLayout = new QVBoxLayout();
    m_pSCVLayout->setSizeConstraint(QVBoxLayout::SetMinAndMaxSize);

    QWidget* widget = new QWidget(this);
    widget->setMinimumSize(72, 32);
    widget->setMaximumSize(80,32);

    FILE * fp;
    char cmd[1024] = {0};
    if(fp = fopen(this->srcDir,"r")){
        fclose(fp);
    }
    else{
        cout << "src file in not here" << endl;
        return ;
    }
    if(fp = fopen((QString(this->dot_routeDir)+QString("/All.dot")).toLatin1().data(),"w+")){
        fclose(fp);
    }
    else{
        sprintf(cmd, (QString("touch ")+QString(this->dot_routeDir)+QString("/All.dot")).toLatin1().data());
        system(cmd);
    }
    ifstream in(this->srcDir);
    ofstream out((QString(this->dot_routeDir)+QString("/All.dot")).toLatin1().data());
    char cur_Func[100] = {0};
    ofstream cur_Out;
    out << "digraph G{\n  rankdir = UD ;" << endl ;
    char line[1024] = {0};
    in.getline(line,1024);
    while(strlen(line)!=0){
        if(line[0]=='0'){
            int i = 3 ;
            char Node_func[100] = {0} ;
            while(line[i]!='\"'){
                Node_func[i-3] = line[i];
                i ++ ;
            }

            if(strncmp(cur_Func,Node_func,100)!=0){
                if(cur_Out.is_open()){
                    cur_Out << "}" << endl ;
                    cur_Out.close();
                    sprintf(cmd,(QString("dot -Tsvg ")+QString(this->dot_routeDir)+QString("/")+QString(cur_Func)+QString(".dot -o ")+QString(this->svg_routeDir)+QString("/")+QString(cur_Func)+QString(".svg")).toLatin1().data());
                    system(cmd);
                }
                strncpy(cur_Func,Node_func,99);
                if(fp = fopen((QString(this->dot_routeDir)+QString("/")+QString(cur_Func)+QString(".dot")).toLatin1().data(),"w+")){
                    fclose(fp);
                }
                else{
                    sprintf(cmd,(QString("touch ")+QString(this->dot_routeDir)+QString("/")+QString(cur_Func)+QString(".dot")).toLatin1().data());
                    system(cmd);
                }
                cur_Out.open((QString(this->dot_routeDir)+QString("/")+QString(cur_Func)+QString(".dot")).toLatin1().data());


                cur_Out << "digraph " << cur_Func << "{\n  rankdir = UD ;" << endl ;

                QPushButton* pushButton;
                pushButton = new QPushButton(widget);
                pushButton->setMinimumSize(pushButton->size());
                pushButton->setObjectName(QString(cur_Func));
                pushButton->setText(QString(cur_Func));
                connect(pushButton,SIGNAL(clicked()),this,SLOT(FuncButtonClicked()));
                m_pSCVLayout->addWidget(pushButton);
            }

            i += 2 ;
            int flag = i ;
            char Node_num[6] = {0} ;
            while(line[i]!=' '){
                Node_num[i-flag] = line[i];
                i ++ ;
            }
            //get node_Num
            i ++ ;
            flag = i ;
            char Node_type[3] = {0} ;
            while(line[i]!='\0'){
                Node_type[i-flag] = line[i];
                i ++ ;
            }
            //get node_Type
            out << "  " << Node_num << " ";
            cur_Out << "  " << Node_num << " " ;
            switch (atoi(Node_type)) {
            case -2:
                out << "[shape = pentagon, ";
                cur_Out << "[shape = pentagon, ";
                break;
            case -1:
                out << "[shape = ellipse, ";
                cur_Out << "[shape = ellipse, ";
                break;
            case 0:
                out << "[shape = box, ";
                cur_Out << "[shape = box, ";
                break;
            case 1:
                out << "[shape = Mrecord, ";
                cur_Out << "[shape = Mrecord, ";
                break;
            case 2:
                out << "[shape = diamond, ";
                cur_Out << "[shape = diamond, ";
                break;
            default:
                break;
            }
            out << "label = \"" ;
            cur_Out << "label = \"" ;
            switch (atoi(Node_type)) {
            case 1:
                out << Node_func << "'s Enter\\n" ;
                cur_Out << Node_func << "'s Enter\\n" ;
                break;
            case -2:
                out << Node_func << "'s Revert" ;
                cur_Out << Node_func << "'s Revert" ;
                break;
            case -1:
                out << Node_func << "'s Exit" ;
                cur_Out << Node_func << "'s Exit" ;
                break;
            default:
                break;
            }
            in.getline(line,1024);
            while(line[0]==' '){
                line[strlen(line)-1] = '\0';
                out << line << "\\n" ;
                cur_Out << line << "\\n" ;
                in.getline(line,1024);
            }
            out << "\"];" << endl;
            cur_Out << "\"];" << endl;
        }
        else if(line[0]=='1'){
            int i = 3 ;
            char Edge_func[100] = {0};
            while(line[i]!='\"'){
                Edge_func[i-3] = line[i];
                i ++ ;
            }
            //get func_Name
            char from_Node[6] = {0};
            i += 2;
            int flag = i;
            while(line[i]!=' '){
                from_Node[i-flag] = line[i];
                i ++ ;
            }
            //get from_Node_num
            char to_Node[6] = {0};
            i += 1;
            flag = i;
            while(line[i]!=' '){
                to_Node[i-flag] = line[i];
                i ++ ;
            }
            //get to_Node_num
            char edge_Type[3] = {0};
            i += 1;
            flag = i;
            while(line[i]!='\0'){
                edge_Type[i-flag] = line[i];
                i ++ ;
            }
            //get edge_Type
            out << "  " << atoi(from_Node) << "->" << atoi(to_Node)
                << "[style = solid, label = \"";
            cur_Out << "  " << atoi(from_Node) << "->" << atoi(to_Node)
                << "[style = solid, label = \"";
            switch (atoi(edge_Type)) {
            case -1:
                out << "False\"];" << endl ;
                cur_Out << "False\"];" << endl ;
                break;
            case 1:
                out << "True\"];" << endl;
                cur_Out << "True\"];" << endl;
                break;
            case 0:
                out << "\"];" << endl;
                cur_Out << "\"];" << endl;
                break;
            default:
                break;
            }
            in.getline(line,1024);
        }
    }

    out << "}" << endl ;
    cur_Out << "}" << endl ;
    in.close();
    out.close();
    cur_Out.close();
    sprintf(cmd,(QString("dot -Tsvg ")+QString(this->dot_routeDir)+QString("/")+QString(cur_Func)+QString(".dot -o ")+QString(this->svg_routeDir)+QString("/")+QString(cur_Func)+QString(".svg")).toLatin1().data());
    system(cmd);
    sprintf(cmd,(QString("dot -Tsvg ")+QString((QString(this->dot_routeDir)+QString("/")+QString("All.dot")).toLatin1().data())+QString(" -o ")+QString(this->svg_routeDir)+QString("/")+QString("all.svg")).toLatin1().data());
    system(cmd);

    widget->setLayout(m_pSCVLayout);
    m_pChatListScrollArea->setWidget(widget);
}
